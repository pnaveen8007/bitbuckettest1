//
//  ContentView.swift
//  BitbucketTest
//
//  Created by Naveen Pokala on 16/02/21.
//  Copyright © 2021 Naveen Pokala. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack{
            Text("Hello World Pushed From Bitbucket")
            Text("Hello World Pushed From Bitbucket")
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
